import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';

import HomeScreen from '../screens/HomeScreen';
import SettingsScreen from '../screens/LoginGoogle';
import LoginScreen from '../screens/LoginGoogle';
import NotificationsScreen from '../screens/NotificationsScreen';


const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios'
          ? `ios-home${focused ? '' : '-outline'}`
          : 'ios-home'
      }
    />
  ),
};

const NotificationsStack = createStackNavigator({
  Notifications: NotificationsScreen,
});

NotificationsStack.navigationOptions = {
  tabBarLabel: 'Graficos',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios'
          ? `md-pie${focused ? '' : '-outline'}`
          : 'md-pie'
      }
    />
  ),
};

const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
});

SettingsStack.navigationOptions = {
  tabBarLabel: 'Login',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios'
          ? `md-contacts${focused ? '' : '-outline'}`
          : 'md-contacts'
      }
    />
  ),
};

// const LoginStack = createStackNavigator({
//   Settings: LoginScreen,
// });

// LoginStack.navigationOptions = {
//   tabBarLabel: 'Login',
//   tabBarIcon: ({ focused }) => (
//     <TabBarIcon
//       focused={focused}
//       name={Platform.OS === 'ios'
//           ? `md-contacts${focused ? '' : '-outline'}`
//           : 'md-contacts'
//       }
//     />

//   ),
// };



export default createBottomTabNavigator({
  HomeStack,
  NotificationsStack,
   SettingsStack,
  // LoginScreen,
  

});
