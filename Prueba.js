
import React, { Component } from 'react';
import { AppRegistry, StyleSheet, TextInput, View, Alert, Button } from 'react-native';

constructor(props) {
 
    super(props)
 
    this.state = {
 
      TextInputName: '',
      TextInputEmail: '',
      TextInputPhoneNumber: ''
 
    }
 
  }

  const { TextInputName }  = this.state ;
  const { TextInputEmail }  = this.state ;
  const { TextInputPhoneNumber }  = this.state ;
  
 fetch('https://reactnativecode.000webhostapp.com/submit_user_info.php', {
   method: 'POST',
   headers: {
     'Accept': 'application/json',
     'Content-Type': 'application/json',
   },
   body: JSON.stringify({
  
     name: TextInputName,
  
     email: TextInputEmail,
  
     phone_number: TextInputPhoneNumber
  
   })
  
 }).then((response) => response.json())
       .then((responseJson) => {
  
 // Showing response message coming from server after inserting records.
         Alert.alert(responseJson);
  
       }).catch((error) => {
         console.error(error);
       });
  
  
   }


const styles = StyleSheet.create({
 
MainContainer :{
 
justifyContent: 'center',
flex:1,
margin: 10
},
 
TextInputStyleClass: {
 
textAlign: 'center',
marginBottom: 7,
height: 40,
borderWidth: 1,
// Set border Hex Color Code Here.
 borderColor: '#FF5722',
 
// Set border Radius.
 //borderRadius: 10 ,
}
 
});