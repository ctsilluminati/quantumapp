

import React from "react"
import { StyleSheet, Text, View, Image, Button,  ImageBackground,TouchableOpacity, } from "react-native"
import Expo from "expo"


export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      signedIn: false,
      name: "",
      photoUrl: ""
    }
  }
  signIn = async () => {
    try {
      const result = await Expo.Google.logInAsync({
        androidClientId:
          "833456763323-ig9ndr0tbvb62jv4ddn6j8pos3a49m35.apps.googleusercontent.com",
        //iosClientId: YOUR_CLIENT_ID_HERE,  <-- if you use iOS
        scopes: ["profile", "email"]
      })

      if (result.type === "success") {
        this.setState({
          signedIn: true,
          name: result.user.name,
          photoUrl: result.user.photoUrl
        })
      } else {
        console.log("cancelled")
      }
    } catch (e) {
      console.log("error", e)
    }
  }
  render() {
    return (
      <View style={styles.container}>
        {this.state.signedIn ? (
          <LoggedInPage name={this.state.name} photoUrl={this.state.photoUrl} />
        ) : (
          <LoginPage signIn={this.signIn} />
        )}
      </View>
    )
  }
}

const LoginPage = props => {
  return (
    <ImageBackground source={require('../assets/images/base.png')} style={{ width: '100%', height: '100%' }}>
        <View style={styles.generalContainer}>
            <View style={styles.buttonsContainer}>
                <View style={styles.loginButtonContainer}>
                    <TouchableOpacity style={[styles.loginButton, styles.buttonAsContainer]}>
                        <Text style={styles.buttonText}  onPress={() => props.signIn()}>LOGIN</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.socialMediaContainer}>
                    <TouchableOpacity style={[styles.socialMediaButtons, styles.buttonAsContainer]}>
                        <Image           
                            style={styles.socialMediaImage}                         
                            source={require('../assets/images/facebook.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.socialMediaButtons, styles.buttonAsContainer]}>
                        <Image  
                            style={styles.socialMediaImage}                                                           
                            source={require('../assets/images/google-plus.png')}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.socialMediaButtons, styles.buttonAsContainer]}>
                        <Image                                    
                            style={styles.socialMediaImage}                         
                            source={require('../assets/images/twitter.png')}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    </ImageBackground>

)
}

const LoggedInPage = props => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Welcome:{props.name}</Text>
      <Image style={styles.image} source={{ uri: props.photoUrl }} />
    </View>
  )
}
let styles = StyleSheet.create({
  generalContainer: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'flex-end',
  },
  buttonsContainer: {
      height: '40%',
      width: '100%',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
  },
  loginButtonContainer: {
      width: '100%',
      height: '40%',
      justifyContent: 'center',
      alignItems: 'center'
  },
  socialMediaContainer: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
      width: '100%',
      height: '40%'
  },
  buttonAsContainer: {        
      height: '50%',
      borderRadius: 30,
      justifyContent: 'center',
      alignItems: 'center'
  },
  loginButton: {
      backgroundColor: 'white',
      width: '90%'
  },
  socialMediaButtons: {
      width: '23%',
      borderColor:'white',
      borderWidth: 0.5
  },
  buttonText: {
      color: 'red'
  },
  socialMediaImage:{
      resizeMode:'center'
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    fontSize: 25
  },
  image: {
    marginTop: 15,
    width: 150,
    height: 150,
    borderColor: "rgba(0,0,0,0.2)",
    borderWidth: 3,
    borderRadius: 150
  }
});


