import React, { Component } from 'react'
import {
    PieChart
} from 'react-native-chart-kit'


export default class CircularByArea extends Component {
    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    render() {        
        let chartConfig = {
            color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
        }
        
        let data=this.props.data.map(item=>{
            return ({name:item.area,population:item.number,color:this.getRandomColor(),legendFontColor: '#7F7F7F', legendFontSize: 8})
        })
        /*let data=[
            { name: 'Internos', population: this.props.data[0].value, color: '#7F7F7F', legendFontColor: '#7F7F7F', legendFontSize: 10 },
            { name: 'Externos', population: this.props.data[1].value, color: 'red', legendFontColor: '#7F7F7F', legendFontSize: 10 },
        ]*/
        return (
            <PieChart
                data={data}
                width={250}
                height={150}
                chartConfig={chartConfig}
                accessor="population"
                backgroundColor="transparent"
            />
        )
    }
}
