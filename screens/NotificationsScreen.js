import React, { Component } from 'react';
import {
    ScrollView,
    StyleSheet,
    View,
    Text
} from 'react-native';


import CircularChartComponent from './CircularChartComponent';
import BarChartComponent from './BarChartComponent';
import { Card } from 'react-native-elements'
import ChartInternExtern from './ChartInternExtern'
import CircularByArea from './CircularByArea';
import CircularByAgent from './CircularByAgent';

export default class CardList extends Component {
    constructor(props){
        super(props);
        this.state={
            scholarshipPercentage:[],
            inerExtern:[],
            data1:[],
            agentes:[]
        }
    }
    componentWillMount(){                
        //Let's wait for the response
        /*let response=await fetch("https://localhost:3000/cont50", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body:  JSON.stringify('')
        }).catch(function(error) {
            console.log('Hubo un problema con la petición Fetch:' + error.message);
        });*/

        let scholarshipPercentage=[{"value": 51},{"value": 49}]        
        let interExtern=[{"value": 41},{"value": 59}];
        let data1=[{"area": "Campus","number": 9},
            {"area": "Capacitación Becarios","number": 11},
            {"area": "CT-WEB","number": 11},
            {"area": "Entrenamiento Becarios","number": 9},
            {"area": "Envio Masivos","number": 10},
            {"area": "Intervención Ágil","number": 3},
            {"area": "Kernel-CT-Santander","number": 8},
            {"area": "Ordenación Académica","number": 12},
            {"area": "Servicios","number": 14},
            {"area": "Sin Asignar","number": 6},
            {"area": "Tecnologías Educativas","number": 7}
          ];
        let agentes=[{"agent": "Gerardo Hernandez","number": 16},
            {"agent": "Juan Tortajada","number": 17},
            {"agent": "Lázaro Hernández","number": 28},
            {"agent": "Lucía Teran","number": 21},
            {"agent": "Silvia Aparicio","number": 12},
            {"agent": "Sin Agente Sin Agente","number": 6}
          ]
          this.setState({
            scholarshipPercentage:scholarshipPercentage,
            interExtern:interExtern,
            data1:data1,
            agentes:agentes
          });
    }
    render() {                            
        return (
            <View style={{ flex: 1 }}>
                <ScrollView contentContainerStyle={{ paddingVertical: 20 }} style={styles.generalContainer}>
                    <Card key={1}>
                        <Text style={styles.chartText}>Porcentaje de becas</Text>
                        <BarChartComponent data={this.state.scholarshipPercentage} />
                    </Card>
                    <Card key={2}>
                        <Text style={styles.chartText}>Status Becarios</Text>
                        <ChartInternExtern data={this.state.interExtern}/>
                    </Card>
                    <Card key={3}>
                        <Text style={styles.chartText}>Becarios por área</Text>
                        <CircularByArea data={this.state.data1}/>
                    </Card>
                    <Card key={4}>
                        <Text style={styles.chartText}>Becarios por gestor</Text>
                        <CircularByAgent data={this.state.agentes}/>
                    </Card>
                </ScrollView>
            </View>
        )
    }
}

let styles = StyleSheet.create({
    generalContainer: {     
        backgroundColor: '#fffaea'        
    },
    chartText: {
        fontSize: 20,
        color: 'red'
    }
})
