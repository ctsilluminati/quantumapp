import React, { Component } from 'react'
import {
    PieChart
} from 'react-native-chart-kit'


export default class CircularChartComponent extends Component {

    render() {
        const data = [
            { name: 'Seoul', population: 21500000, color: 'rgba(131, 167, 234, 1)', legendFontColor: '#7F7F7F', legendFontSize: 15 },
            { name: 'Toronto', population: 2800000, color: '#F00', legendFontColor: '#7F7F7F', legendFontSize: 15 },
            { name: 'Beijing', population: 527612, color: 'red', legendFontColor: '#7F7F7F', legendFontSize: 15 },
            { name: 'New York', population: 8538000, color: '#ffffff', legendFontColor: '#7F7F7F', legendFontSize: 15 },
            { name: 'Moscow', population: 11920000, color: 'rgb(0, 0, 255)', legendFontColor: '#7F7F7F', legendFontSize: 15 }
        ]
        let chartConfig = {                        
            color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,            
          }
        //console.log("Data que recibe es: ",this.props.data[0].value);
        /*let data=[
            { name: 'Internos', population: this.props.data[0].value, color: '#7F7F7F', legendFontColor: '#7F7F7F', legendFontSize: 10 },
            { name: 'Externos', population: this.props.data[1].value, color: 'red', legendFontColor: '#7F7F7F', legendFontSize: 10 },
        ]*/
        return (
            <PieChart
                data={data}
                width={250}
                height={150}
                chartConfig={chartConfig}
                accessor="population"
                backgroundColor="transparent"                
            />
        )
    }
}
