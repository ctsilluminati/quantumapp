import React, { Component } from 'react'
import {
    BarChart
} from 'react-native-chart-kit'



export default class BarChartComponent extends Component {

    render() {
        let res=this.props.data.map(item=>{return (item.value)});        
        const data = {
            labels: ['50%', '100%'],
            datasets: [{
              data: res
            }]
        }
        const chartConfig = {
            backgroundGradientFrom: '#e3efe9',
            backgroundGradientTo: '#e3efe9',       
            bgColor:'white',     
            color: (opacity = 1) => `rgba(66, 134, 244,1)`            
        }

        
        return (
            <BarChart   
                bgColor='white'             
                data={data}                
                height={220}
                width={300}
                chartConfig={chartConfig}
            />
        )
    }
}
