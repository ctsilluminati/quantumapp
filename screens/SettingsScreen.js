
import React from "react"
import { StyleSheet, Text, View, Image, Button, Dimensions } from "react-native"
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph
} from 'react-native-chart-kit'

const screenWidth = Dimensions.get('window').width

const colors = ['#FFBE9B', '#E8D68B', '#C9FFA6', '#81E8CC', '#9DB7FF', '#ABE87D', '#99FFFF', '#E88C88', '#B897FF', '#99FFFF','#FFB14A'];

const chartConfig = {
    backgroundColor: '#e26a00',
    backgroundGradientFrom: '#fb8c00',
    backgroundGradientTo: '#ffa726',
    decimalPlaces: 2, // optional, defaults to 2dp
    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    style: {
      borderRadius: 16
    }
}

export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      signedIn: false,
      name: "",
      photoUrl: ""
    }
  }
  componentDidMount() {
    fetch('http://172.27.14.161/quantumvalley/public/infoapp')
      .then(response => response.json())
      .then(res => {

        console.log(res);
        const { data1 } = res; 
        const { data2 } = res; 
        this.parseData(data1, data2) 
        
      
      });
  }

  /*
   * 
   */
  getName(data, id){
    let i=0;
    for (i; i< data.length; i++){      
      if (data[i].id === id) return data[i].name;
    }
  }
  parseData(data, names){
    // parse data
    let dataReady = [];
    var i = 0;
    for (i; i< data.length; i++){
      dataReady.push({ name: this.getName(names, data[i].idArea), population: data[i].conteo, color: colors[i], legendFontColor: '#7F7F7F', legendFontSize: 15});
    }
    const parsedData = dataReady;
    this.setState({ data: parsedData }) 
  }


  
  render() {
    return (
      <View style={styles.container}>
        <PieChart
        data={this.state.data}
        width={screenWidth}
        height={220}
        chartConfig={chartConfig}
        accessor="population"
        backgroundColor="transparent"
        paddingLeft="5"
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    fontSize: 25
  },
  image: {
    marginTop: 15,
    width: 150,
    height: 150,
    borderColor: "rgba(0,0,0,0.2)",
    borderWidth: 3,
    borderRadius: 150
  }
})