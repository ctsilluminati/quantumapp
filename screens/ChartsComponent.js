import React, { Component } from 'react';
import {
    ScrollView,
    StyleSheet,
    View,
    Text
} from 'react-native';


import CircularChartComponent from './CircularChartComponent';
import BarChartComponent from './BarChartComponent';

export default class ChartsComponent extends Component {

    render() {        

        return (
            <ScrollView  style={styles.generalContainer} contentContainerStyle={{ paddingVertical: 20 }}>
                <View style={styles.cardContainer}>
                    <View style={styles.card}>
                        <View>
                            <Text style={styles.chartText}>Porcentaje de Becas</Text>
                        </View>
                        <View style={styles.chartContainer}>
                            <BarChartComponent />
                        </View>
                    </View>
                </View>
                <View style={styles.cardContainer}>
                    <View style={styles.card}>
                        <View>
                            <Text style={styles.chartText}>Porcentaje de Becas</Text>
                        </View>
                        <View style={styles.chartContainer}>
                            <CircularChartComponent />
                        </View>
                    </View>
                </View>
                <View style={styles.cardContainer}>
                    <View style={styles.card}>
                        <View>
                            <Text style={styles.chartText}>Porcentaje de Becas</Text>
                        </View>
                        <View style={styles.chartContainer}>
                            <CircularChartComponent />
                        </View>
                    </View>
                </View>                               
            </ScrollView>
        )
    }
}

let styles = StyleSheet.create({
    generalContainer: {
        padding: '7%',
        backgroundColor: '#fffaea',
        height:100        
    },
    cardContainer: {
        height: '40%',
        width: '100%',        
        padding: '2%'
    },
    card: {
        backgroundColor: 'white',
        padding: '5%',
        borderRadius: 20,
        borderColor: 'black',
        borderWidth: 0.5,
        height: '100%',
        width: '100%'
    },
    chartText: {
        fontSize: 20,
        color: 'red'
    },
    chartContainer: {
        height: '100%',
        width: '100%'
    },
})
